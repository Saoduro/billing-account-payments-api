var requestBody = JSON.parse(context.getVariable("request.content"));
print(requestBody.billingId);

var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

print(requestBody.payment.datePosted)
 context.setVariable("date_try",requestBody.payment.datePosted); 

var date = moment(requestBody.payment.datePosted);
var formats = "YYYY-MM-DDTHH:mm:ss.SSS";
if(typeof requestBody.payment.dateTimeReceived !== 'undefined') {
    var dateTime = moment(requestBody.payment.dateTimeReceived, formats, true);
    if(!dateTime.isValid()) {
        context.setVariable("isRequestValid", false); 
        context.setVariable("path", "/properties/payment/dateTimeReceived/Invalid");
        context.setVariable("errorMessage", "Invalid dateTimeReceived is provided.");
        context.setVariable("customErrorDetail", "Invalid dateTimeReceived is provided.");
    }
}

if(!date.isValid()) {
   context.setVariable("isRequestValid", false); 
   context.setVariable("path", "/properties/payment/datePosted/Invalid");
   context.setVariable("errorMessage", "Invalid datePosted is provided.");
   context.setVariable("customErrorDetail", "Invalid datePosted is provided.");
  
}

var path = "/billingaccounts/" + requestBody.billingId + "/payments";

print(path);
context.setVariable("apiPath",path);

var billingId = requestBody.billingId;

context.setVariable("billingId",billingId);

