    
var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.path = "/billingaccounts/" + context.getVariable('request.queryparam.billingId') + "/payments/";
http.keyMappingsUrl = "http://"+context.getVariable("private.keymapping-eks-ip")+"/api/v2/customers/"+context.getVariable("request.queryparam.billingId")+"/keylookup?idtype=BILLING_ID";
http.apikey = context.getVariable("private.apikey");

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var input = {};
input.attributes = attributes;

var opa_request = {};
opa_request.input = input

context.setVariable("opa_request", JSON.stringify(opa_request));
context.setVariable("data.api.path","/system/authorize_billingid");