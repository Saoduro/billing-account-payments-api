 var correlationId = context.getVariable('request.header.X-Correlation-ID');
var timeStamp = context.getVariable('currentSystemTimeStamp');
context.setVariable("CorrelationId", correlationId);
context.setVariable("timeStamp", timeStamp);
var startDate = context.getVariable('request.queryparam.startDate');
var endDate = context.getVariable('request.queryparam.endDate');
var billingId = context.getVariable('request.queryparam.billingId');

context.setVariable("invalidParam", "false");
var billigIdRegExp = /^[0-9]/;
var regex = new RegExp("^[0-9]{4,}$");

if (billingId === null ) {
    context.setVariable("invalidParam", "true");
    context.setVariable("errorMessage", "Please provide a valid billingId (numeric value and minimum length should be 4)");
    context.setVariable("queryParam", "billingId");
    
} else if (billingId !== null) {
    var billingIds = billingId.split(",");

    (function validateBillingId() {
        
        for( var id=0; id<billingIds.length; id++) {
            
            if(!billigIdRegExp.test(billingIds[id])) {
                context.setVariable("errorMessage", "Please provide a valid billingId (numeric value and minimum length should be 4)");
                context.setVariable("queryParam", "billingId");
                context.setVariable("invalidParam", "true");
                return "false";
                
            } else if( !regex.test(billingIds[id]) ) {
                context.setVariable("invalidParam", "true");
                context.setVariable("queryParam", "billingId");
                context.setVariable("errorMessage", "String is too short (" + billingIds[id].length + " chars), minimum 4");

            return "false";
            }
        }
    }())
    
} 
var invalidParam = context.getVariable('invalidParam');
if (invalidParam === "false") {
    
    if (startDate !== null && !moment(startDate).isValid()) { 
        context.setVariable("invalidParam", "true");
        context.setVariable("errorMessage", "Please provide valid startDate"); 
        context.setVariable("queryParam", "startDate");
    
    } else if (endDate !== null && !moment(endDate).isValid()) { 
        context.setVariable("invalidParam", "true");
        context.setVariable("errorMessage", "Please provide valid endDate"); 
        context.setVariable("queryParam", "endDate");
    

    }
}
var last = context.getVariable("request.queryparam.last");
var regexLast = new RegExp("^(true|false|TRUE|FALSE|True|False)$");

if(last !== null && !regexLast.test(last)) {
    context.setVariable("invalidParam", "true");
    context.setVariable("queryParam", "last");
    context.setVariable("errorMessage", "Invalid last is provided.");
} else if (regexLast.test(last)) {
    context.setVariable("request.queryparam.last", last.toLowerCase());
}