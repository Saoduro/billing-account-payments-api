var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.path = context.getVariable("apiPath");
http.keyMappingsUrl =  "http://"+context.getVariable("private.keymapping-eks-ip")+"/api/v2/customers/"+context.getVariable("billingId")+"/keylookup?idtype=BILLING_ID";
http.apikey = context.getVariable("private.apikey");

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var input = {};

input.attributes = attributes;

var opa_request = {};
opa_request.attributes = attributes


context.setVariable("opa_request", JSON.stringify(opa_request));