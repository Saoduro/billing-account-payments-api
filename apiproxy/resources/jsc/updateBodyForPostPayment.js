var requestBodyAPI = JSON.parse(context.getVariable("request.content"));
var JWTinjection = JSON.parse(context.getVariable("jwt.DJ_DecodeJWT.payload-json"));
var finalRequest = {};

if(requestBodyAPI !== null) {
    
    finalRequest.correlationId = requestBodyAPI.correlationId;
    finalRequest.checkDigitId = requestBodyAPI.checkDigitId;
    finalRequest.billingId = requestBodyAPI.billingId;
    if (requestBodyAPI.email !== null)
    finalRequest.email = requestBodyAPI.email;
     else {
    finalRequest.email  = JWTinjection.email;
    }
    finalRequest.payment = requestBodyAPI.payment;
    finalRequest.bankAccount = requestBodyAPI.bankAccount;
    
}

context.setVariable("finalRequestString", JSON.stringify(finalRequest));